'use strict';

var myApp = angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/coffees.html',
    controller: 'CoffeeController'
  });
}])


